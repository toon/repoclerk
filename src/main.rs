use actix_web::{
    http::StatusCode,
    middleware, middleware::Logger,
    web::{self, Buf, BytesMut, Payload},
    App, HttpMessage, HttpRequest, HttpResponse, HttpResponseBuilder, HttpServer,
};
use flate2::read::GzDecoder;
use futures_util::StreamExt;
use std::{
    env,
    io::{Read, Write},
    path::Path,
    process::{Command, Stdio},
};

async fn handle_git(req: HttpRequest, mut payload: Payload) -> HttpResponse {
    let mut input = BytesMut::new();
    while let Some(chunk) = payload.next().await {
        input.extend_from_slice(chunk.unwrap().chunk());
    }

    let body: Vec<u8> = if req.headers().contains_key("content-encoding") {
        let mut decoder = GzDecoder::new(&input[..]);
        let mut decompressed_body = Vec::new();
        decoder.read_to_end(&mut decompressed_body).unwrap();
        decompressed_body.to_vec()
    } else {
        input.to_vec()
    };

    let path_info = "/".to_owned() + req.match_info().query("tail");
    let connection_info = req.connection_info();
    let remote_addr = connection_info.peer_addr().unwrap();
    let content_type = req.content_type();
    let query_string = req.query_string();
    let request_method = req.method().as_str();
    let git_protocol = req.headers().get("git-protocol").unwrap().to_str().unwrap();

    let bundle_uri = hostaddr()
        + "/bundle/"
        + Path::new(&path_info)
            .parent()
            .unwrap()
            .file_stem()
            .unwrap()
            .to_str()
            .unwrap();

    let mut cmd = Command::new("git")
        .arg("-c")
        .arg("uploadpack.advertiseBundleURIs=true")
        .arg("-c")
        .arg("bundle.version=1")
        .arg("-c")
        .arg("bundle.mode=all")
        .arg("-c")
        .arg("bundle.some.uri=http://".to_owned() + bundle_uri.as_str() + ".bundle")
        .arg("http-backend")
        .env("PATH_INFO", path_info)
        .env("GIT_HTTP_EXPORT_ALL", "")
        .env("HTTP_GIT_PROTOCOL", git_protocol)
        .env("REMOTE_ADDR", remote_addr)
        .env("CONTENT_TYPE", content_type)
        .env("QUERY_STRING", query_string)
        .env("REQUEST_METHOD", request_method)
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn()
        .expect("Failed to spawn child process");

    if !body.is_empty() {
        let stdin = cmd.stdin.as_mut().unwrap();
        stdin.write_all(&body).unwrap();
    }

    let output = cmd.wait_with_output().unwrap();
    let bytes = output.stdout.to_vec();

    if !output.status.success() {
        eprintln!("stderr: {}", String::from_utf8_lossy(&output.stderr));
    }

    let mut builder = HttpResponseBuilder::new(StatusCode::OK);
    let mut code = StatusCode::OK.as_u16();

    let mut headers = [httparse::EMPTY_HEADER; 100];
    let header = httparse::parse_headers(&output.stdout, &mut headers)
        .map_err(|e| HttpResponse::InternalServerError().body(e.to_string()))
        .unwrap();

    match header {
        httparse::Status::Complete((_, raw_headers)) => {
            for raw_header in raw_headers {
                match raw_header.name {
                    "Status" => {
                        let strcode = String::from_utf8(raw_header.value[..3].to_vec()).unwrap();
                        code = strcode.parse::<u16>().unwrap();
                    }
                    _ => {
                        let val = raw_header.value.to_vec();
                        builder.append_header((raw_header.name, val));
                    }
                }
            }
        }
        httparse::Status::Partial => {
            return HttpResponse::InternalServerError().body("Incomplete headers")
        }
    };
    let offset = header.unwrap().0;
    let code = StatusCode::from_u16(code).unwrap();
    builder.status(code);

    let body = bytes[offset..].to_vec();
    builder.body(body)
}

fn hostaddr() -> String {
    env::var("HOST").unwrap()
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::init_from_env(env_logger::Env::new().default_filter_or("info"));

    HttpServer::new(|| {
        App::new()
            .wrap(middleware::Compress::default())
            .wrap(Logger::default())
            .service(web::scope("/git").route("{tail:.*}", web::to(handle_git)))
            .service(
                actix_files::Files::new("/bundle/", env::var("GIT_BUNDLE_PATH").unwrap())
                    .show_files_listing(),
            )
    })
    .bind(hostaddr())?
    .run()
    .await
}
