# RepoClerk

This is a simple implementation of a HTTP server hosting Git repositories over
the [Smart HTTP protocol][1]. It pipes requests into [`git-http-backend(1)`][2]
and returns it's response.

Next to serving git repositories, it also hosts git bundles using the
[bundle-URI][3] mechanism.

## Usage

You can run the server with `cargo run`. You're supposed to set a few
environment variables:

- `HOST` :: The host address to listen on, for example `127.0.0.1:8080`.
- `GIT_PROJECT_ROOT` :: The path git repositories are stored.
- `GIT_BUNDLE_PATH` :: The path where bundles for the git repositories are
  stored. The server assumes bundles are named `<name>.bundle` when the
  repositories are named `<name>.git`.

The git repositories are available inside the `/git` sub-directory.

When you run the server you can clone a repository with the command:

```shell
git clone http://127.0.0.1:8080/git/example.git
```

The server will advertise a bundle at
`http://127.0.0.1:8080/bundle/example.bundle`. If you want the client to use
these bundles, then you need to set the git-config(1) `transfer.bundleURI=true`.

## Limitations

- Pushes are not supported at the moment.
- Authentication is not supported at the moment.

[1]: https://www.git-scm.com/docs/gitprotocol-http#_smart_clients
[2]: https://git-scm.com/docs/git-http-backend
[3]: https://git-scm.com/docs/bundle-uri/
